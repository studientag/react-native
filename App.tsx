import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Chat from './src/components/chat/Chat';
import { useEffect, useState } from 'react';
import getMessages from './src/api/intercom/getMessages';
import getThread from './src/api/intercom/getThread';
import { config } from './src/types/TConfig';
import getMemberFromThread from './src/utils/getMemberFromThread';
import { TThread } from './src/types/TThread';
import { TMessage } from './src/types/TMessage';
import LoadingCursor from './src/components/loading/LoadingCursor';

export default function App() {
    const [thread, setThread] = useState<TThread>();
    const [messages, setMessages] = useState<TMessage[]>();

    useEffect(() => {
        void handleLoad();
    }, []);

    const handleLoad = async () => {
        const thread = await getThread(config.threadId);
        if (!thread) return;
        const member = getMemberFromThread({
            thread,
            userId: config.userId
        })
        if (!member) return;

        const messages = await getMessages(thread.id, member.id);

        if (!messages) return;

        setMessages(messages)
        setThread(thread);
    };
  return (
      <>
          {
              thread && messages ? <Chat messages={messages} thread={thread}/> : <LoadingCursor/>
          }
      </>
  );
}
