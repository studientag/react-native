import { TThread } from '../types/TThread';
import { TMember } from '../types/TMember';

type getMemberParams = {
    thread: TThread;
    id?: string;
    userId?: string;
}

export default ({ thread, userId , id}: getMemberParams): TMember | undefined => {
    if (!userId && !id) return undefined;
    return thread.members.find((member) => {
        if (id && member.id === id) return true;
        return 'tobitId' in member && member.tobitId.toString() === userId;
    });
}
