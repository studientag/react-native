import React, { FC, useEffect, useState } from 'react';
import { TMessage } from '../../../types/TMessage';
import { StyleSheet, Text, View } from 'react-native';
import getMemberFromThread from '../../../utils/getMemberFromThread';

type MessageProps = {
    message: TMessage;
    isLastIndex: boolean
}

const styles = StyleSheet.create({
    body: {
        margin: 8,
        padding: 8,
        borderRadius: 5,
        backgroundColor: '#E2E8F0'
    },
    author: {
        fontWeight: 'bold',
        paddingBottom: 3
    }
})

const Message: FC<MessageProps> = ({ message, isLastIndex }) => {
    return (
        <View style={styles.body}>
            <Text style={styles.author}>{message.author.name}</Text>
            <Text>{message.text}</Text>
        </View>
    )
}

export default Message;

Message.displayName = 'Message'
