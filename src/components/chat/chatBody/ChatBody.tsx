import React, { FC, useEffect, useRef } from 'react';
import { FlatList, ScrollView, StyleSheet, View } from 'react-native';
import { TMessage } from '../../../types/TMessage';
import Message from './Message';
import message from './Message';

const styles = StyleSheet.create({
    wrapper: {
        height: 700,
    }
});

type ChatBodyProps= {
    messages: TMessage[];
}


const ChatBody: FC<ChatBodyProps> = ({ messages }) => {
    const list = useRef<ScrollView>();

    useEffect(() => {
        list.current?.scrollToEnd({animated: true});
    }, [])
    return (
        <View>
            <FlatList
                data={messages}
                renderItem={(item) => <Message message={item.item} isLastIndex={item.index === message.length - 1}/> }
                style={styles.wrapper}
                inverted
            />
        </View>
    )
}

export default ChatBody;

ChatBody.displayName = 'ChatBody'
