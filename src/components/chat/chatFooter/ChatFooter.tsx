import React, { FC } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        height: 80,
        // backgroundColor: 'red',
    },
    textInputWrapper: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 8,
        paddingVertical: 16
    },
    textInput: {
        height: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5
    }
})

const ChatFooter: FC = () => {
    return (
        <View style={styles.wrapper}>
            <View style={styles.textInputWrapper}>
                <TextInput placeholder={'Schreibe eine Nachricht...'} style={styles.textInput}/>
            </View>
        </View>
    )
}

export default ChatFooter;

ChatFooter.displayName = 'ChatFooter'
