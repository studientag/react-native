import React, { FC } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import ChatHead from './chatHead/ChatHead';
import { TThread } from '../../types/TThread';
import { TMessage } from '../../types/TMessage';
import ChatFooter from './chatFooter/ChatFooter';
import ChatBody from './chatBody/ChatBody';

type ChatProps = {
    thread: TThread
    messages: TMessage[]
}

const Chat: FC<ChatProps> = ({ thread, messages }) => {
    return (
        <View style={{flex: 1}}>
            {/*<Text style={{ textAlign: 'center' }}>Bitte warten</Text>*/}
            <ChatHead name={thread.name ?? 'Chat Name'}/>
            <ChatBody messages={messages}/>
            <ChatFooter/>
        </View>
    )
}

export default Chat;

Chat.displayName = 'Chat'
