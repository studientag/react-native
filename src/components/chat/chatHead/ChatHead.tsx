import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { COLORS } from '../../../const/colors';

const styles = StyleSheet.create({
    body: {
        height: 60,
        textAlignVertical: 'center'
    },
    background: {
        borderColor: COLORS.border,
        borderBottomWidth: 2
    },
    textWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    name: {
        paddingLeft: 8,
        fontSize: 25,
        fontWeight: 'bold',
        textAlignVertical: 'center'
    }
})

type ChatNameProps ={
    name: string;
}

const ChatHead: FC<ChatNameProps> = ({ name }) => {
    return (
        <View style={[styles.body, styles.background]}>
            <View style={styles.textWrapper}>
                <Text style={styles.name}>{ name }</Text>
            </View>
        </View>
    )
}

export default ChatHead;

ChatHead.displayName = 'ChatHead'
