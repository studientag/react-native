import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { FC } from 'react';

const styles = StyleSheet.create({
    center: {
        textAlign: 'center'
    }
})

const LoadingCursor: FC = () => {
    return (
        <View style={{flex: 1, justifyContent: 'center'}}>
            <ActivityIndicator size={'large'}/>
            <Text style={{ textAlign: 'center', paddingTop: 8 }}>Bitte warten</Text>
        </View>
    )
}

export default LoadingCursor;

LoadingCursor.displayName = 'LoadingCursor';
