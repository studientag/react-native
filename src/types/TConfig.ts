import * as configFile from '../../configs/config.json';

export type TConfig = {
    "accessToken": string,
    "threadId": string,
    "intercomUrl": string,
    userId: string;
};


export const config: TConfig = configFile;
