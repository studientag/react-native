export type TMember = {
    id: string
} & (TUserMember | TLocationMember)

export type TUserMember = {
    tobitId: number;
    firstName: string;
    lastName: string;
}

export type TLocationMember = {
    name: string;
    locationId: number;
}
