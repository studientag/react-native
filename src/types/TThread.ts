import { TMember } from './TMember';

export type TThread = {
    id: string;
    name?: string;
    members: TMember[];
}
