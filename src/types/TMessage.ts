export type TMessage = {
    author: {
        id: string;
        name: string;
    }
    id: string;
    text: string;
}
