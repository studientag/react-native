import { config } from '../../types/TConfig';
import { TThread } from '../../types/TThread';

export default async (threadId: string): Promise<TThread | undefined> => {
    const url = `${config.intercomUrl}/thread/${threadId}`;
    const response = await fetch(url, {
        headers: {
            Authorization: `Bearer ${config.accessToken}`
        }
    });

    if (!response.ok) {
        console.error('failed to load thread');

        return undefined;
    }

    return (await response.json()).thread;
}
