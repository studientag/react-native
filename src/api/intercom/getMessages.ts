import { config } from '../../types/TConfig';
import { TThread } from '../../types/TThread';
import { TMessage } from '../../types/TMessage';

export default async (threadId: string, memberId: string): Promise<TMessage[] | undefined> => {
    const url = `${config.intercomUrl}/thread/${threadId}/messages?memberId=${memberId}&take=20`;
    const response = await fetch(url, {
        headers: {
            Authorization: `Bearer ${config.accessToken}`
        }
    });

    if (!response.ok) {
        console.error('failed to load thread');

        return undefined;
    }

    return (await response.json()).messages as TMessage[];
}
